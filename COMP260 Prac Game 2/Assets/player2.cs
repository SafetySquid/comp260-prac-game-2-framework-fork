﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class player2 : MonoBehaviour
{

    // Use this for initialization
    private Rigidbody rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        //rigidbody.useGravity = false; //potential removal
    }
    public Vector3 move;
    public Vector3 velocity;
    public float maxSpeed = 5.0f;    public float acceleration = 3.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public bool player1;
    public string Vertical;
    public string Horizontal;
    public float turnSpeed = 30.0f; // in degrees/second
    public float force = 10f;


    public Vector3 move2;
    public Vector3 velocity2;
    public float maxSpeed2 = 5.0f;    public float acceleration2 = 3.0f; // in metres/second/second
    private float speed2 = 0.0f; // in metres/second
    public float brake2 = 5.0f; // in metres/second/second



    public float turnSpeed2 = 30.0f; // in degrees/second
    public float force2 = 10f;


    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Time = " + Time.time);
    }
    //public float speed = 20f;
    void FixedUpdate()
    {
        float forwards = Input.GetAxis("Horizontal");
        float upwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                if (speed < 0)
                {
                    speed = 0;
                }
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
                if (speed > 0)
                {
                    speed = 0;
                }
            }
            /*if (speed < brake && speed > 0)
            {
                speed = 0;
            } else if(speed > -brake && speed < 0)
            {
                speed = 0;
            }*/
        }
        if (upwards > 0)
        {
            // accelerate forwards
            speed2 = speed2 + acceleration2 * Time.deltaTime;
        }
        else if (upwards < 0)
        {
            // accelerate backwards
            speed2 = speed2 - acceleration2 * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed2 > 0)
            {
                speed2 = speed2 - brake2 * Time.deltaTime;
                if (speed2 < 0)
                {
                    speed2 = 0;
                }
            }
            else
            {
                speed2 = speed2 + brake2 * Time.deltaTime;
                if (speed2 > 0)
                {
                    speed2 = 0;
                }
            }
            /*if (speed < brake && speed > 0)
            {
                speed = 0;
            } else if(speed > -brake && speed < 0)
            {
                speed = 0;
            }*/
        }


        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        Vector3 velocity = Vector3.left * speed;
        //second method
        Vector3 pos = new Vector3(rigidbody.position.x/* + 0.1f*/, rigidbody.position.y, rigidbody.position.z);


        if (forwards != 0)
        {
            pos.x = rigidbody.position.x + 1;
        }
        if(upwards != 0)
        {
            pos.z = rigidbody.position.z + 1;
        }
        Vector3 dir = pos - rigidbody.position;

        dir = dir.normalized;
       // rigidbody.velocity = dir * speed;

        
        //if (forwards != 0 || upwards != 0)
        //{
        rigidbody.velocity = dir * speed;

        rigidbody.velocity = dir * speed2;
        //rigidbody.position = pos;
        //}

        /*
        speed2 = Mathf.Clamp(speed2, -maxSpeed, maxSpeed);


        Vector3 velocity2 = Vector3.forward * speed2;
        //second method
        Vector3 pos2 = new Vector3(rigidbody.position.x, rigidbody.position.y, rigidbody.position.z + 0.1f);
        Vector3 dir2 = pos2 - rigidbody.position;
        dir2 = dir2.normalized;
        //rigidbody.velocity = dir2 * speed2;
        
        if (upwards > 0 || upwards < 0)
        {
            rigidbody.velocity = dir2 * speed2;
        }
        //rigidbody.velocity2 = dir2 * speed2;
        //rigidbody.velocity = dir * speed;
        // rigidbody.velocity = dir2 * speed2;
        */



        /* first method
        rigidbody.velocity = velocity * speed;
        // move the object

        rigidbody.AddForce(velocity * force);
        */
        //rigidbody.MovePosition(velocity);

        // compute a vector in the up direction of length speed
        /*
        Vector2 velocity = Vector2.left * speed;
        
        rigidbody.velocity = velocity * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
        */
        /*if (speed < brake && speed > 0)
        {
            speed = 0;
        } else if(speed > -brake && speed < 0)
        {
            speed = 0;
        }*/
    }



    //Vector3 pos = GetMousePosition();
    //Vector3 dir = pos - rigidbody.position;
    //Vector3 vel = dir.normalized * speed;
    // check is this speed is going to overshoot the target
    //float move = speed * Time.fixedDeltaTime;
    //float distToTarget = dir.magnitude;
    //if (move > distToTarget)
    //{
    // scale the velocity down appropriately
    //vel = vel * distToTarget / move;
    //}
    //rigidbody.velocity = vel;
}
    /*
    private Vector3 GetMousePosition()
    {

        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    */
//}