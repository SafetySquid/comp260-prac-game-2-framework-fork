﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleAI : MonoBehaviour {
    // Use this for initialization
    private Rigidbody rigidbody;
    public Rigidbody puckBody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        //rigidbody.useGravity = false; //potential removal
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Time = " + Time.time);
    }
    public Transform puckPos;
    public float speed = 20f;
    void FixedUpdate()
    {
        Vector3 pos = puckPos.transform.position;
        //pos.x = pos.x + 0.2f;
        Vector3 rigidPos = rigidbody.transform.position;
        //pos.x += 0.3f;
        //pos.z += 0.4f;
        pos.x += 0.4f;
        if(pos.x < 0)
        {
            pos.x = 0;
        }
        //if(pos.x - 1 > rigidPos.x && pos.z < rigidPos.z + 1 && pos.z > rigidPos.z -1)
        //{
        //    pos.z += 4;
        //    pos.x += 1;
        //}
        
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;
        //Vector3 pos = GetMousePosition();
        //Vector3 dir = pos - rigidbody.position;
        //Vector3 vel = dir.normalized * speed;
        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;
    }
    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
