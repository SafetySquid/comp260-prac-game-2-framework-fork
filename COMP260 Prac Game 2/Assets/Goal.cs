﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {

    // Use this for initialization
    public AudioClip scoreClip;
    private AudioSource audio;
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }
    void OnTriggerEnter(Collider collider)
    {
        // play score sound
        audio.PlayOneShot(scoreClip);
        PuckControl puck =
 collider.gameObject.GetComponent<PuckControl>();
 puck.ResetPosition();
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
