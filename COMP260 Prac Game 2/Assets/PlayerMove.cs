﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {


    public Vector2 move;
    public Vector2 velocity;
    public float maxSpeed = 5.0f;    public float acceleration = 3.0f; // in metres/second/second
    //private float speed = 0.0f; // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public bool player1;
    public string Vertical;
    public string Horizontal;
    public float turnSpeed = 30.0f; // in degrees/second



    // Use this for initialization
    private Rigidbody rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        //rigidbody.useGravity = false; //potential removal
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Time = " + Time.time);
    }
    public float speed = 20f;
    public float force = 10f;
    void FixedUpdate()
    {
        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            Vector3 pos = GetMousePosition();
            Vector3 dir = pos - rigidbody.position;
            rigidbody.AddForce(dir.normalized * force);
            // accelerate forwards
            //speed = speed + acceleration * Time.deltaTime;
        }
        
        
        /*
        Vector3 pos = GetMousePosition();
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;
        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;
        */
    }
    private Vector3 GetMousePosition()
    {
        // create a ray from the camera
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
